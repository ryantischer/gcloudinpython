
import subprocess

# gcloud config set project tischer-179116


def display_project_details():
    print ("current project is: ")
    cmd = "gcloud config list --format 'value(core.project)' 2>/dev/null"
    try:
        subprocess.call(cmd, shell=True)

        print("")
        

    except:
        print("An exception occurred. Gcloud Installed?")
        exit()

        try:
            print("Current VPCs:")
            cmd = "gcloud compute networks list"
            subprocess.call(cmd, shell=True)
            print ("")
            print ("Name must be lowercase letters, numbers, and hyphens and must end with a number or letter")
        except:
            print("An exception occurred.  Gcloud Installed?")
            exit()

    return "something"


def build_vpc(*argv):

    display_project_details()
    if __name__ != "__main__":
        params = [item for item in argv]
        if len(params) == 4:
            output = "gcloud compute networks create {} --description={} --bgp-routing-mode={} --subnet-mode={}".format(params[0], params[1], params[2], params[3])
            print output
            return output
        else:
            print ("paramiters list is not = 4.  must be build_vpc(VPC NAME, VPC DESC, ROUTING MODE, SUBNET MODE)")

    else:

        vpcName = raw_input("Enter new VPC name: ")
        print ("")

        vpcDesc = raw_input("Enter VPC desctiption: ")
        print ("")

        subnetMode = raw_input("Enter Enter for custom subnets or 1 for auto: ")
        if len(subnetMode) == 0:
            subnet = "custom"
        else:
            subnet = "auto"
        print ("")

        routingMode = raw_input("Press Enter for global routing or 2 for regional: ")
        if len(routingMode) == 0:
            route = "global"
        else:
            route = "regional"
        print ("")

        print("")
        print ("Your Gcloud Command is:")
        output = "gcloud compute networks create {} --description={} --bgp-routing-mode={} --subnet-mode={}".format(vpcName, vpcDesc, route, subnet)
        print(output)

        return None



def build_subnet(*argv):

    display_project_details()
    if __name__ != "__main__":
        params = [item for item in argv]
        if params[4] == "1":
            private = " --enable-private-ip-google-access"
        else:
            private = ""

        if params[5]  == "1":
            flow = " --enable-flow-logs"
        else:
            flow = ""

        if len(params) == 6:
            output = "gcloud beta compute networks subnets create {} --network={} --region={} --range={}{}{}".format(params[0],params[1] , params[2], params[3], private, flow)
            return output
        else:
            print ("paramiters list is not = 6.  must be build_subnet(subnetName, vpcName, region, range, private, flow)")
            print ("* = private access enabled (1 to enable) ** = flow logs enabled (1 to enable")

    else:
        vpcName = raw_input("Enter VPC Name: ")
        print ("")

        subnetName = raw_input("Enter subnet Name: ")
        print ("")

        # US-East4 and US-West2
        regionName = raw_input("Enter Region.  Press 1 for US-East4, 2 for US-West2 or enter region name: ")
        if regionName == "1":
            region = "us-east4"
        elif regionName == "2":
            region = "us-west2"
        else:
            region = regionName
        print ("")

        range = raw_input("Enter ip address range.  Example 192.168.1.1/24: ")
        print ("")

        privateAccess = raw_input("Press Enter to enable Private Access or 1: ")
        if len(privateAccess) == 0:
            private =" --enable-private-ip-google-access"
        print ("")

        flowLogs = raw_input("Press Enter to enable flowlogs or press enter: ")
        if len(flowLogs) == 0:
            flow = " --enable-flow-logs"
        print ("")

        output = "gcloud beta compute networks subnets create {} --network={} --region={} --range={}{}{}".format(subnetName, vpcName, region, range, private, flow)
        print output

        return output


def build_route():

    display_project_details()
    vpcName = raw_input("Enter VPC Name: ")
    print ("")

    routeName = raw_input("Enter Route Name: ")
    print ("")

    destRange = raw_input("Enter destination range.  Example 192.168.1.1/24: ")
    print ("")

    routeDesc = raw_input("Enter Route Description: ")
    print ("")

    routePriority = raw_input("Enter Route Priortiy: ")
    print ("")

    routeTags = raw_input("Enter Route Tags: ")
    print ("")

    routeNH = raw_input("Press Enter for Internet route or enter IP address: ")
    if len(routeNH) == "0":
        route = "default-internet-gateway"
    else:
        route = routeNH
    print ("")

    output = "gcloud beta compute routes create {} --description={} --network={} --priority={} --tags={} --destination-range={} --next-hop-gateway={}".format(routeName, routeDesc, vpcName, routePriority, routeTags, destRange, route)
    print output
    return output


def build_firewall():
    return None


def build_dns():
    return None


if __name__ == "__main__":
    print("" * 10)
    print ("This software assumes that Gcloud SDK is installed")
    print ("")
    print ("What would you like to build?")
    print ("")
    print ("1.  Create and list VPCs")
    print ("2.  Create and list subnets within an exsiting VPC")
    print ("3.  Create and list routes")
    print ("4.  Create and list firewall rules - WIP")
    print ("5.  Create and list DNS zones - WIP")
    usrInput = raw_input("Enter your Choice: ")
    val = str(usrInput)

    if val == "1":
        build_vpc()

    elif val == "2":
        build_subnet()

    elif val == "3":
        build_route()

    elif val == "4":
        build_subnet()

    elif val == "5":
        build_subnet()

    elif val == "6":
        build_subnet()
    print ("")
    print ("")
