import GCPNet
import subprocess

# usage
# GCPNet can be run directly to output GCloud command
# GCPNet_main leverages GCPnet.py to automate running multiple commands in seq
# Note: Nothing here checks the output of the Gcloud commands.
# All of this should be replaced with Google python SDK

# ideas for this scipt
# 1.  Convert to Python SDK
# 2.  take input from cli params
# 3.  Build in 

# vpc and subnet details
# BEGIN USER INPUT


vpcName = "testvpc1"
vpcDesc = "'some desc'"
subnetMode = "custom"
routingMode = "global"


subnetName = "testsubnet"
regionName = "us-east4"
range = "172.16.21.0/24"
# Enter 1 to enable private access and flow log, 0 to disable
privateAccess = "1"
flowLogs = "1"

# END USER INPUT

#Build a vpc with a subnet
try:
    cmd = GCPNet.build_vpc(vpcName, vpcDesc, routingMode, subnetMode,)
    subprocess.call(cmd, shell=True)
    cmd = GCPNet.build_subnet(subnetName, vpcName, regionName, range, "1", "1")
    subprocess.call(cmd, shell=True)

except:
    print("An exception occurred. Gcloud Installed?")
    exit()
